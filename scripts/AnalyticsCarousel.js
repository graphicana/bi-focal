$(document).ready(function(){
    $('.carrousel_left').click(function(){
        var elementsCount=$('carrousel_inner ul li').size();
        var position =$('.carrousel').attr('data-pos');
        position=parseInt(position, 4);
        elementsCount=elementsCount+3;
        if (position<elementsCount)
        {
            position=position+1;
            $('.carrousel_right').removeClass('right_inactive');
            if (position==elementsCount){
                $(this).addClass('left_inactive');
            }
            var pos=position*230;
            $('.carrousel').attr('data-pos',position);
            $('.carrousel_inner').animate({'scrollLeft' : pos });
        }
    });

    $('.carrousel_right').click(function(){
        var position =$('.carrousel').attr('data-pos');
        var elementsCount=$('carrousel_inner ul li').size();
        position=parseInt(position, 10);
        if (position>0)
        {
            $('.carrousel_left').removeClass('left_inactive');
            position=position-1;
            if(position==0)
                {
                    $(this).addClass('right_inactive');
                }
            var pos=position*230;
            $('.carrousel').attr('data-pos',position);
            $('.carrousel_inner').animate({'scrollLeft' : pos });
        }
    });
});

$(document).ready(function(){
    $('.month-carrousel_left').click(function(){
        var elementsCount=$('month-carrousel_inner ul li').size();
        var position =$('.month-carrousel').attr('data-pos');
        position=parseInt(position, 15);
        elementsCount=elementsCount+1;
        if (position<elementsCount)
        {
            position=position+1;
            $('.month-carrousel_right').removeClass('right_inactive');
            if (position==elementsCount){
                $(this).addClass('left_inactive');
            }
            var pos=position*230;
            $('.month-carrousel').attr('data-pos',position);
            $('.month-carrousel_inner').animate({'scrollLeft' : pos });
        }
    });

    $('.month-carrousel_right').click(function(){
        var position =$('.month-carrousel').attr('data-pos');
        var elementsCount=$('month-carrousel_inner ul li').size();
        position=parseInt(position, 10);
        if (position>0)
        {
            $('.month-carrousel_left').removeClass('left_inactive');
            position=position-1;
            if(position==0)
            {
                $(this).addClass('right_inactive');
            }
            var pos=position*230;
            $('.month-carrousel').attr('data-pos',position);
            $('.month-carrousel_inner').animate({'scrollLeft' : pos });
        }
    });
});