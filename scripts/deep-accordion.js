(function (jQuery) {
    jQuery.fn.extend({
        accordion: function () {
            return this.each(function () {

                var $ul = $(this);

                if ($ul.data('accordiated'))
                    return false;

                $.each($ul.find('ul, li>div'), function () {
                    $(this).data('accordiated', true);
                    $(this).hide();
                });

                $.each($ul.find('a'), function () {
                    $(this).click(function (e) {
                        activate(this);
                        return void(0);
                    });
                });

                var active = $('#side-bar .active');

                if (active) {
                    activate(active, 'toggle');
                    $(active).parents().show();
                }

                $('.accordion > li > a').click(function () {
                    $('#side-bar .modal-side-bar').removeClass('col-md-5');
                    $('#side-bar .modal-side-bar').addClass('col-md-3');
                    $('#side-bar .modal-body').addClass('col-md-12');
                    $('#side-bar .modal-body').removeClass('col-md-7');
                })
                $('.accordion .has-child').click(function () {
                    $('#side-bar .modal-side-bar').toggleClass('col-md-5');
                    $('#side-bar .modal-side-bar').toggleClass('col-md-3');
                    $('#side-bar .modal-body').toggleClass('col-md-12');
                    $('#side-bar .modal-body').toggleClass('col-md-7');
                })

                function activate(el, effect) {
                    if (!effect) {
                        $(el)
                            .toggleClass('active')
                            .parent('li')
                            .siblings()
                            .find('a')
                            .removeClass('active')
                            .parent('li')
                            .children('ul, div')
                            .slideUp('fast');
                    }
                    $(el)
                        .siblings('ul, div')[(effect || 'slideToggle')]((!effect) ? 'fast' : null);
                }

            });
        }
    });
})(jQuery);

$('ul.accordion').accordion();