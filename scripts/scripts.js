$(function () {
    var body = document.body,
        html = document.documentElement,
        windowHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight);
    screenHeight = document.body.offsetHeight;

    console.log(windowHeight);
    $(".details").attr('style', "min-height:" + (screenHeight - 43) + "px;");
    $(".filter-bar").attr('style', "max-height:" + (screenHeight - 80) + "px;");

    //$('.kpis .section:nth-child(3)').attr('style', 'margin-right:0;');

    $('.file-menu ul.menu').hide();
    $('.file-menu .trigger').click(function () {
        $('.file-menu .menu').hide();
        $(this).attr('style', 'visiblity:hidden;');
        $(this).parent('.file-menu').children('ul.menu').slideDown(300);
        $('.back-lay').toggle();
    })
    $('.file-menu li:first-child').click(function () {
        $(this).parent().parent('.file-menu').children('.trigger').slideDown(300);
        $(this).parent().slideUp(300);
        $('.back-lay').toggle();
    })

    $('.sidebar-toggle').click(function () {
        $('.left-bar-switch').toggle();
        $('.back-lay').toggle();
    })

    $('.filter-bar .filter-toggle').click(function () {
        $(this).parent().parent().toggleClass('col-sm-1').toggleClass('col-sm-4');
        $(this).parent().parent().toggleClass('col-xs-1').toggleClass('col-xs-8');
        $('.back-lay').toggle();
    })
    $('.left-bar-switch .glyphicon-filter').click(function () {
        if ($('.left-bar-switch').hasClass('col-sm-1')) {
            console.log('true');
            $(this).parent().parent().parent().parent().removeClass('col-sm-1').addClass('col-sm-4');
            $(this).parent().parent().parent().parent().removeClass('col-xs-1').addClass('col-xs-8');
        }
    })
    $('.left-bar-switch .glyphicon-refresh').click(function () {
        if ($('.left-bar-switch').hasClass('col-sm-1')) {
            console.log('true');
            $(this).parent().parent().parent().parent().removeClass('col-sm-1').addClass('col-sm-4');
            $(this).parent().parent().parent().parent().removeClass('col-xs-1').addClass('col-xs-8');
        }
    })
    $('.back-lay').click(function () {
        $(this).hide();
        $('.left-bar-switch').addClass('col-sm-1').removeClass('col-sm-4').addClass('col-xs-1').removeClass('col-xs-8');
        $('.file-menu .menu').slideUp(300);
    });

    $('#collapseFilters li .filter-content').hide();
    $('#collapseFilters h4').click(function () {
        $(this).parent().children('.filter-content').toggle();
        $(this).toggleClass('open');
    });

    $(function () {
        $('#profile-edit a:first').tab('show')
    })

    $('#user-info').collapse({
        parent: false
    })

    $.fn.extend({
        filterTable: function () {
            return this.each(function () {
                $(this).on('keyup', function (e) {
                    $('.filterTable_no_results').remove();
                    var $this = $(this), search = $this.val().toLowerCase(), target = $this.attr('data-filters'), $target = $(target), $rows = $target.find('tbody tr');
                    if (search == '') {
                        $rows.show();
                    } else {
                        $rows.each(function () {
                            var $this = $(this);
                            $this.text().toLowerCase().indexOf(search) === -1 ? $this.hide() : $this.show();
                        })
                        if ($target.find('tbody tr:visible').size() === 0) {
                            var col_count = $target.find('tr').first().find('td').size();
                            var no_results = $('<tr class="filterTable_no_results"><td colspan="' + col_count + '">No results found</td></tr>')
                            $target.find('tbody').append(no_results);
                        }
                    }
                });
            });
        }
    });
    $('[data-action="filter"]').filterTable();

    $(function () {
        // attach table filter plugin to inputs
        $('[data-action="filter"]').filterTable();

        $('.container').on('click', '.panel-heading span.filter', function (e) {
            var $this = $(this),
                $panel = $this.parents('.panel');

            $panel.find('.panel-body').slideToggle();
            if ($this.css('display') != 'none') {
                $panel.find('.panel-body input').focus();
            }
        });
        $('[data-toggle="tooltip"]').tooltip();
    })


//checkbox styling
    $.fn.toggleSwitch = function (options) {
        var settings = $.extend({
            onClick: function () {
            },
            onChangeOn: function () {
            },
            onChangeOff: function () {
            }
        }, options);
        $(this).each(function (i) {
            var obj = $(this), status = obj.is(':checked') ? '' : ' off';
            if (!obj.parent('div.switch').length) {
                obj.wrap('<div class="switch"></div>');
                obj.parent('div.switch').prepend('<span class="switched' + status + '" />').prepend('<div class="overlay" />');
            }
            obj.parent('div.switch').add($('label[for=' + obj.prop('id') + ']')).click(function (e) {
                e.preventDefault();
                if (!obj.prop('disabled')) {
                    var value, check;
                    settings.onClick.call(obj);
                    if ($(this).is('label')) {
                        value = $('#' + $(this).prop('for')).prev('span.switched');
                        check = $('#' + $(this).prop('for'));
                    } else {
                        value = $(this).children('span.switched');
                        check = $(this).children('input[type=checkbox]');
                    }
                    if (value.is('.off')) {
                        value.stop().animate({left: 0}, 150, 'linear').removeClass('off');
                        check.prop('checked', 'checked');
                        settings.onChangeOn.call(obj);
                    } else {
                        value.stop().animate({left: -58}, 150, 'linear').addClass('off');
                        check.prop('checked', '');
                        settings.onChangeOff.call(obj);
                    }
                }
            });
        });
    };

    //chart scripts
    $('.data .graph-selection .glyphicon-remove').click(function () {
        $(this).parent('.graph-selection').removeClass('hide');
    })
    $('.data .graph-selection .btn').click(function () {
        $(this).parent('.graph-selection').addClass('hide');
    })
    $('.file-menu .menu .glyphicon-tasks').click(function () {
        $(this).parent().parent().slideUp(300);
        $('.graph-selection').show(300);
    })

    /**/
})